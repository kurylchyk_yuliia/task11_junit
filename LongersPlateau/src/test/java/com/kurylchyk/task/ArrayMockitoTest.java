package com.kurylchyk.task;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.*;


@RunWith(MockitoJUnitRunner.class)
public class ArrayMockitoTest {

    int[] arr = new int[]{1, 9, 9, 1, 5, 9, 9, 9, 5, 8};
    @Mock
    CalcWithArray calcWithArray;

    @InjectMocks
    ArrayInt arrayInt  = new ArrayInt(arr);


    @Test
    public void TestArrayInt(){

        int[] a = new int[]{1,2,3,4,5,7};
        Mockito.when(calcWithArray.setNewArray(arr,a)).thenReturn(a);

        assertEquals(arrayInt.setNewArray(a),a);


        Mockito.verify(calcWithArray).setNewArray(arr,a);
    }

    @Test
    public void TestArraySize(){
        Mockito.when(calcWithArray.getSize(arr)).thenReturn(arr.length);

        assertEquals(arrayInt.getSize(),arr.length);

        Mockito.verify(calcWithArray).getSize(arr);

    }
}
