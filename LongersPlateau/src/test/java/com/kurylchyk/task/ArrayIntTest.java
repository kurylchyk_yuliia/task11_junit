package com.kurylchyk.task;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.RepetitionInfo;

import java.io.File;
import java.io.IOException;

import static org.junit.Assert.*;


public class ArrayIntTest {

   private ArrayInt arrayInt;


    /**
     * Has to check the appropriate index
     * if the input is not suite for template
     * (last and first index has to be 0)
     */
    @Test
    public void checkConditionIsNotRight() {
        int[] array = {1, 9, 9, 10, 5, 9, 9, 9, 10, 8};
        arrayInt = new ArrayInt(array);
        arrayInt.findEqualSequence();
        assertTrue(arrayInt.getIndexFirst() == 0);
        assertTrue(arrayInt.getIndexLast() == 0);
    }

    /**
     * Has to check the appropriate index
     * if the input is suitable for template
     * (last and first index has to be 0)
     */
    @Test
    public void checkConditionIsRight() {
        int[] array = {1, 9, 9, 1, 5, 9, 9, 9, 5, 8};
        arrayInt = new ArrayInt(array);
        arrayInt.findEqualSequence();
        assertTrue(arrayInt.getIndexLast() != 0);
        assertTrue(arrayInt.getLengthOfEqualsElement() != 0);
    }


    /**
     * Check if array's values are random generated
     * @param testInfo
     */
    @DisplayName("Checking the value of array")
    @RepeatedTest(3)
        public void initArr(TestInfo testInfo,RepetitionInfo repetitionInfo){
        System.out.println("Running repeated test -> " + repetitionInfo.getCurrentRepetition());
        ArrayInt arrayInt1 = new ArrayInt(5);
        ArrayInt arrayInt2 = new ArrayInt(5);
        assertNotEquals(arrayInt1,arrayInt2);
    }


    @Test
    public void testPackagePrivate(){
        ArrayInt arrayInt = new ArrayInt(5);
        assertNotEquals(arrayInt.getArray(), null);
    }

    /**
     * Has to have at least 1 obj
     */
    @Test
    public void getCreatedObj(){
        arrayInt = new ArrayInt(5);
        assertNotEquals(ArrayInt.getCreatedObj(),0);
    }


    @Test
    public void creationFile() throws IOException {
        File newFile = new File("src/test/newFile.txt");
        boolean success = newFile.createNewFile();
        assertTrue(success);
    }
}


