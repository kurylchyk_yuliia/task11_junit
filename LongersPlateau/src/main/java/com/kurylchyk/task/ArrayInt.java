package com.kurylchyk.task;
import java.util.Random;

public class ArrayInt {

    private int[] array;
    private int lengthOfEqualsElement;
    private int indexFirst;
    private int indexLast;
    private Random random = new Random();
    private static int  createdObj = 0;
    private CalcWithArray calcWithArray;

    public ArrayInt(int [] arr){
        createdObj++;
        this.array = arr;
        lengthOfEqualsElement = 0;
        indexFirst = 0;
        indexLast = 0;
    }

    final static int  getCreatedObj(){
        return createdObj;
    }
    public ArrayInt(int size){
        createdObj++;

        if(size==0)
            size++;
        array = new int[size];

        for(int index = 0; index< array.length; index++){
            array[index] = random.nextInt(20)+1;
        }
    }

    final int getSize(){
       return calcWithArray.getSize(array);
       // return this.array.length;
    }
    public void findEqualSequence(){

        if(array == null) {
            throw new NullPointerException();
        }

        int currentLength  = 0;
        int first = 0;
        int last = 0;

        for(int index = 0; index<array.length-1;){



            if(array[index]==array[index+1]) {
                currentLength = 1;
                first = index;
                last = index;

                while (array[index] == array[index + 1]) {

                    index++;
                    last = index;
                    currentLength++;
                }

                if (array[first-1]<array[first] && array[last+1]<array[last]) {
                    if (currentLength > lengthOfEqualsElement) {
                        lengthOfEqualsElement = currentLength;
                        indexLast = last;
                        indexFirst = first;
                    }
                }
            }
            index++;
        }


    }

    public int getIndexFirst(){
        return indexFirst;
    }
    public int getIndexLast(){
        return indexLast;
    }

    public int getLengthOfEqualsElement() {
        return lengthOfEqualsElement;
    }

    int[] getArray(){
        return array;
    }



  public int [] setNewArray(int[] arr){
        if(arr == null) {
            return null;
        } else {

          return calcWithArray.setNewArray(array,arr);

        }
  }

}
