package com.kurylchyk.task;

public interface CalcWithArray {

    int getSize(int[] arr);


    int[] setNewArray(int[] from, int[] to);
}
