package com.kurylchyk.task;
import org.junit.jupiter.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class RepeatedMineTest {

    @Parameterized.Parameter(0)
    public int M;
    @Parameterized.Parameter(1)
    public int N;

@Parameterized.Parameters

    public static Collection input(){

    return Arrays.asList(new Object[][]{{4,4},{2,5},{6,5},{6,6}});
}

@Test
    public void differentParameterTest() throws IncorrectValue {
    Assertions.assertTrue((new Minesweeper(M,N)).getCountOfElements()==M*N);
}


}
