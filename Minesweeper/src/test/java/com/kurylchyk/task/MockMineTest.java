package com.kurylchyk.task;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.jupiter.api.Assertions.assertTrue;

@RunWith(MockitoJUnitRunner.class)
public class MockMineTest {

    @Mock
    CalcArray calcArray;
    @InjectMocks
    Minesweeper minesweeper =  new Minesweeper(4,4);

    public MockMineTest() throws IncorrectValue {
    }

    @Test
    public void testPercentage() throws IncorrectValue {

        boolean[][] arr = new boolean[4][4];

       Mockito.when(calcArray.checkPercent(arr)).thenReturn(4);

        assertTrue(minesweeper.checkPercent()<=4);

    }
}
