package com.kurylchyk.task;
import org.junit.Test;
import org.junit.jupiter.api.BeforeAll;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MinesweeperTest {

    @BeforeAll
    public void print(){
        System.out.println("Testing the minesweeper");
    }

    @Test
     public void  testSize0(){
        int m = 0;
        int n = 0;
        assertThrows(IncorrectValue.class,()->{new Minesweeper(m,n);});
    }

    @Test
    public void testSize1(){
        int m =1;
        int n = 1;
        assertThrows(IncorrectValue.class,()->{new Minesweeper(m,n);});
    }

    /**
     * The percentage should not be bigger then expected
     */
    @Test
    public void checkPercentage() throws IncorrectValue {
        int m = 4;
        int n = 4;
        assertTrue((new Minesweeper(m,n)).getPercentage()<=m+n/2);

    }





}
