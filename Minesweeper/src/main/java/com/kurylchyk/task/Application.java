package com.kurylchyk.task;

public class Application {

    public static void main(String[] args) throws IncorrectValue {

        Minesweeper minesweeper = new Minesweeper(4,4);
        minesweeper.replaceEachBelow();
        minesweeper.fillRepresentation();
        minesweeper.getRepresentation();

    }
}
