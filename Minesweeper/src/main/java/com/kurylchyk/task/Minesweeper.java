package com.kurylchyk.task;
import com.kurylchyk.task.CalcArray;
import com.kurylchyk.task.IncorrectValue;

import java.util.Random;
public class Minesweeper {

    private int M;
    private int N;
    private boolean[][] field;
    private char[][] representation;
    private int[][] replacingValue;
    private Random random = new Random();
    private int p;
    private CalcArray calcArray;


    public Minesweeper(int m, int n) throws IncorrectValue {

       if(m<=0&& n<=0)
           throw new IncorrectValue();

       if(m==1 && n==1){
           throw new IncorrectValue();
       }


       M= m;
       N = n;
       setPercentage();
       field = new boolean[M][N];
       representation = new char[M][N];
       replacingValue = new int[M][N];
       fillField();
    }

    private void setPercentage(){

        p = M+N/2;

    }

    public int getPercentage(){
        return p;
    }
    private void fillField() {

        boolean value;
        int countOfBomb = 0;
        for (int index = 0; index < M; index++) {

            for (int jdex = 0; jdex < N; jdex++) {

                value = random.nextInt(2) == 1 ? true : false;
                if (value==false && countOfBomb <= p) {
                    field[index][jdex] = value;
                    countOfBomb++;
                } else {
                    field[index][jdex] = true;
                }
            }
        }
    }

    public int getCountOfElements(){
        return M*N;
    }

    public int checkPercent() {
        return calcArray.checkPercent(field);
    }


    public void fillRepresentation(){

        for(int index = 0; index<M; index++){
            for(int jdex = 0; jdex<N; jdex++){
                representation[index][jdex] = field[index][jdex]==true?'◘':'*';
            }
        }

        for(int index = 0; index<M; index++){
            for(int jdex=0 ; jdex<N; jdex++) {

                if(field[index][jdex]==true && replacingValue[index][jdex]!=0){
                    representation[index][jdex] =(char)(replacingValue[index][jdex]+'0');

                }
            }
        }
    }


    public void replaceEachBelow(){

        int countOfNeighborBomb = 0;


        for(int index = 0; index<M; index++){
            for(int jdex = 0; jdex< N; jdex++){

                if(field[index][jdex]==true){
                    int currentIndex = index+1;

                    for(;currentIndex<M;currentIndex++){
                        if (field[currentIndex][jdex] == false) {
                            countOfNeighborBomb++;
                        }

                    }
                    replacingValue[index][jdex] = countOfNeighborBomb;
                    countOfNeighborBomb = 0;
                }
            }
        }
    }



    public void getRepresentation(){

        for(char[] x :representation){
            for(char y:x){
                System.out.print(y+"\t");
            }
            System.out.println("");
        }

    }

}
